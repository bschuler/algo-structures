#!/usr/bin/python3

#each node knows its children but ignore its value
#children are dictionnaries with the letter of the children as key and the child TrieNode as value
#each terminal letter TrieNode contains a key * with no value

class TrieNode:
  def __init__(self):
    self.children = {}

class Trie:

  def __init__(self):
    self.root = TrieNode()
  
  def search(self, word):
    """Searches for a word or prefix in the tree. Return None if it is not found and the node associated to its last letter otherwise

    Args:
        word (str): a word or prefix to search in the tree

    Returns:
        TrieNode: The node associated to the last letter
    """
    currentNode = self.root
    for letter in word:
      if not letter in currentNode.children.keys():
        return None
      else:
        currentNode = currentNode.children[letter]
    return currentNode
  
  def insert(self, word):
    """Add a word in the tree

    Args:
        word (str): a word to add in the tree
    """
    currentNode = self.root
    for letter in word:
      if not letter in currentNode.children.keys():
        nextNode = TrieNode()
        currentNode.children[letter] = nextNode
        currentNode = nextNode
      else:
        currentNode = currentNode.children[letter]
    currentNode.children['*'] = None
  
  def collectAllWords(self, node=None, word="", words=[]):
    """A recursive function searching through a given tree for all words.

    Args:
        node (TrieNode, optional): The starting node of the search. Defaults to None.
        word (str, optional): Here we write the word as we find it recursively. You can set a prefix if you are searching for suffixes only. Defaults to "".
        words (list, optional): The words that have been found. DISCLAIMER: if you don't start this function from the tree node, please specify [] as an argument. Do not trust the default. Defaults to [].

    Returns:
        list: the list of words that have been found
    """
    
    if node is None:
      node = self.root
      words = []
    
    for letter in node.children.keys():
      if letter != '*':
        self.collectAllWords(node.children[letter], word+letter, words)
    if '*' in node.children.keys():
      words.append(word)
    return words

  def autocomplete(self, prefix = ''):
    """A function searching through a given tree for all words with a given prefix

    Args:
        prefix (str): a prefix to the words we are searching for. No prefix by default

    Returns:
        list: the list of words that have been found having the prefix
    """
    currentNode = self.search(prefix)
    if not currentNode:
      return None
    return self.collectAllWords(currentNode, prefix, [])


def searchprint(value, searchedPattern = "The searched pattern"):
  if value is None:
    print(searchedPattern," doesn't exist in the tree")
  else:
    print(searchedPattern," has been found in the tree")

yggdrasil = Trie()
yggdrasil.insert('viking')
yggdrasil.insert('vikings')

searchprint(yggdrasil.search('armagueddon'))
searchprint(yggdrasil.search('viking'))
searchprint(yggdrasil.search('vikings'))

print(yggdrasil.collectAllWords())
print(yggdrasil.collectAllWords())

yggdrasil.insert('drakkar')
yggdrasil.insert('viceroy')
yggdrasil.insert('dragon')

searchprint(yggdrasil.search('dra'))

yggdrasil.insert('viking')
yggdrasil.insert('valhalla')
yggdrasil.insert('dragonfire')
yggdrasil.insert('9qsdam!@#%!^&')

print(yggdrasil.collectAllWords())

print(yggdrasil.autocomplete('viking'))
print(yggdrasil.autocomplete('dra'))
print(yggdrasil.autocomplete('vi'))
print(yggdrasil.autocomplete())