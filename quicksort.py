#!/usr/bin/python3

import numpy as np

arrayTest1 = [0,5,2,1,6,3]
arrayTest2 = [0,3,1]
arrayTest3 = np.random.randint(25, size=(10))

def quicksort(arrayParameterInput):
  """ Function implementing the quicksort algorithm to sort an array of numbers recursively with the help of a number chosen as pivot at each iteration"""

  arrayParameter = arrayParameterInput.copy()
  
  ##base cases : return None if there is nothing to return or the array if it contains a single member
  if (len(arrayParameter) == 0):
    return

  elif (len(arrayParameter) == 1):
    return arrayParameter

  ## the pivot is the last value of the list. We set a leftPointer on the first Element of the list and a rightPointer on the last which isn't the pivot
  else:
    pivot = len(arrayParameter) - 1
    pivotValue = arrayParameter[pivot]
    leftPointer = 0
    rightPointer = len(arrayParameter) - 2

    ## the left pointers increases and the right pointer decreases until they meet
    while(leftPointer < rightPointer):

      ## they move until they find a value that is misplaced in relation to the pivot (unless they meet the other pointer before)
      while(arrayParameter[leftPointer] < pivotValue and leftPointer < rightPointer):
        leftPointer +=1
      
      while(arrayParameter[rightPointer] >= pivotValue and leftPointer < rightPointer):
        rightPointer -=1
      
      ## then they swap those values. If everything was well placed they are equal, so it changes nothing
      arrayParameter = swap(arrayParameter, leftPointer, rightPointer)   

    ##Now the pointer met where the pivot should be! We check wether it was not in place already
    if (arrayParameter[leftPointer] >= pivotValue):

      ## it wasn't : we can swap the pivo with the pointer
      arrayParameter = swap(arrayParameter, leftPointer, pivot)

      ## finally the recursive calls : we know the pivot is at its right place. So we can recall the function left and right from it, and add each nonempty part
      result = np.array([arrayParameter[leftPointer]])
      leftmember = quicksort(arrayParameter[0: leftPointer])
      rightmember = quicksort(arrayParameter[leftPointer+1: len(arrayParameter)])
      
      if not (leftmember is None):
        result = np.concatenate((leftmember, result), axis=None)
      if not (rightmember is None):
        result = np.concatenate((result, rightmember), axis =None)

      return result

    ## here I handle the case were the pivot was well placed already. It differs from above because the leftPointer does not become the pivot's position, and thus is still misplaced
    else: 
      result = np.array([arrayParameter[pivot]])
      leftmember = quicksort(arrayParameter[0: pivot])

      if not (leftmember is None):
        result = np.concatenate((leftmember, result), axis=None)
      return result


def swap(arrayParameter, left, right):
  temp = arrayParameter[left]
  arrayParameter[left] = arrayParameter[right]
  arrayParameter[right] = temp
  return arrayParameter


def pseudoprint(arrayParameterInput, status='step'):
  print('#### print ' + status+ ' ####')
  print(arrayParameterInput)


def quickselect(arrayParameterInput, nMinimumValues):
  """Function returning unsorted the n lowest element of an array of numbers. It is based on the quicksort algorithm"""

  arrayParameter = arrayParameterInput.copy()
  
  assert nMinimumValues > 0, "You need to provide a strictly positive number, which is the amount of lowest value of the list you will retrieve"

  ## If we have enough values already we return them
  if(nMinimumValues >= len(arrayParameterInput)):
    return arrayParameter
  
  if (len(arrayParameter) == 0):
    return

  elif (len(arrayParameter) == 1):
    return arrayParameter

  else:
    pivot = len(arrayParameter) - 1
    pivotValue = arrayParameter[pivot]
    leftPointer = 0
    rightPointer = len(arrayParameter) - 2

    while(leftPointer < rightPointer):
      while(arrayParameter[leftPointer] < pivotValue and leftPointer < rightPointer):
        leftPointer +=1
      
      while(arrayParameter[rightPointer] >= pivotValue and leftPointer < rightPointer):
        rightPointer -=1
      
      if(leftPointer < rightPointer):
        arrayParameter = swap(arrayParameter, leftPointer, rightPointer)   

    if (arrayParameter[leftPointer] >= pivotValue):

      arrayParameter = swap(arrayParameter, leftPointer, pivot)

      ## If we have too many values we recall the function on a subset of the array (we know the pointer isn't part of it)
      if (leftPointer+1 > nMinimumValues):
        return quickselect(arrayParameter[0: leftPointer], nMinimumValues)     

      ## if we have exactly enough values we return them all
      elif(leftPointer+1 == nMinimumValues):
        return arrayParameter[0: leftPointer+1]

      ## if we don't have enough values, we know every value of the left side is lower than the pivot, so they're all good enough.
      ## We take those and call the function with the number of values we're missing on the right side
      else:
        result = arrayParameter[0: leftPointer+1]
        leftmember = quickselect(arrayParameter[leftPointer+1: len(arrayParameter)], nMinimumValues - leftPointer -1)
        return np.concatenate((result, leftmember), axis = None)

    else:
      ## the pivot was already well placed on the right, but we didn't have enough values. So we recall without the pivot 
      return quickselect(arrayParameter[0: pivot], nMinimumValues)


pseudoprint(arrayTest3, 'start')
resultsort = quicksort(arrayTest3)
resultselect = quickselect(arrayTest3, 6)
pseudoprint(resultsort, 'sort end')
pseudoprint(resultselect, 'select end')
pseudoprint(arrayTest3, 'post')